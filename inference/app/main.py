"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""

import logging
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger
import tensorflow as tf
import tensorflow_hub as hub
import numpy as np
import os
import json
import pandas as pd
import datetime

__author__ = "### Author ###"

logger = XprLogger("inference",level=logging.INFO)
hub_url = "https://tfhub.dev/google/universal-sentence-encoder/4"


class Inference(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()
        """ Initialize any static data required during boot up """

    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        self.embed = hub.KerasLayer(hub_url)
        path = model_path
        files = os.listdir(path)

        plans = {}
        columns = ['Plan_Name', 'Plan_Type', 'Important Questions', 'Answers', 'Why this Matters:', 'Url']
        self.df = pd.DataFrame(columns=columns)
        counter = 0
        for file in files:
            with open(path + '/' + file, 'r') as filen:
                f = json.load(filen)
                plan_name = f['Plan_Name'].strip()
                plan_type = f['Plan_Type'].strip()
                for rows in f['Page_1']:
                    for each_row in rows:
                        ques = f['Page_1'][each_row]['Important Questions']
                        ques = ques.replace("\n", " ")
                        ques = ques.replace("–", " ")
                        ques = ques.replace("-", " ")
                        ques = ques.replace("?", "")
                        ques = ques.strip()
                        ans = f['Page_1'][each_row]['Answers'].replace("\n", " ")
                        ans = ans.strip()
                        why = f['Page_1'][each_row]['Why this Matters:'].replace("\n", " ")
                        why = why.strip()
                        file_url = file.replace("%", "%25")
                        url = 'http://172.16.6.95:6080/files/data/Ambetter_pdf/' + file_url.split("json")[0] + 'pdf'
                        self.df.loc[counter] = [plan_name] + [plan_type] + [ques] + [ans] + [why] + [url]
                        counter = counter + 1
        questions = self.df['Important Questions'].tolist()
        self.embeddings = self.embed(questions)

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """
        return input_request

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """

        response = {}
        Plan_Name = input_request["plan_name"]
        Plan_Type = input_request["plan_type"]
        query = input_request["query"]
        result_count = input_request["result_count"]
        
        start = datetime.datetime.now()
        results = []
        status = 400
        index_list = self.df.index[(self.df['Plan_Name'] == Plan_Name) & (self.df['Plan_Type'] == Plan_Type)].to_numpy()
        if (index_list is not None) and (len(index_list) > 0) and result_count > 0:
            if len(index_list) < result_count:
                result_count = len(index_list)
            query_embed = self.embed([query])
            similarity = np.inner(query_embed,self.embeddings)
            similar_scores = similarity[:,index_list][0]
            ranked = np.argsort(similar_scores)
            largest_indices = ranked[::-1][:result_count]
            df_indexes = index_list[largest_indices]
            for i in range(df_indexes.shape[0]):
                result = {}
                result['Answer'] = self.df.loc[df_indexes[i]]['Answers']
                result['Url'] = self.df.loc[df_indexes[i]]['Url']
                result['Score'] = str(similar_scores[np.where(index_list == df_indexes[i])[0][0]])
                results.append(result)
            status = 200
            
        response ['status'] = status
        response ['result_count'] = len(results)
        response ['results'] = results
        
        end = datetime.datetime.now()
        diff = end - start
        elapsed_ms = (diff.days * 86400000) + (diff.seconds * 1000) + (diff.microseconds / 1000)
        print("Query processed in time in milliseconds :", elapsed_ms)
        print(response)
        return response


    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        return output_response


if __name__ == "__main__":
    pred = Inference()
    # === To run locally. Use load_model instead of load. ===
    # pred.load_model(model_path="/model_path/") instead of pred.load()
    pred.load()
    pred.run_api(port=5000)
