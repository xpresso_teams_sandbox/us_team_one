"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

import camelot
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import json
import glob
import os
#%matplotlib inline

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from io import StringIO

__author__ = "### Author ###"

logger = XprLogger("data_preprocessing",level=logging.INFO)


class DataPreprocessing(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="DataPreprocessing")
        """ Initialize all the required constansts and data her """

    def start(self, run_name,input_path, output_path):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """

        def convert_pdf_to_txt(path):
            rsrcmgr = PDFResourceManager()
            retstr = StringIO()
            laparams = LAParams()
            device = TextConverter(rsrcmgr, retstr, laparams=laparams)
            fp = open(path, 'rb')
            interpreter = PDFPageInterpreter(rsrcmgr, device)
            password = ""
            maxpages = 0
            caching = True
            pagenos = set()

            for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching,
                                          check_extractable=True):
                interpreter.process_page(page)

            text = retstr.getvalue()

            fp.close()
            device.close()
            retstr.close()
            return text


        try:
            super().start(xpresso_run_name=run_name)

            input_files = os.listdir(input_path)
            plan_name_type = set()

            result_dict = {}
            for filename in input_files:
                pdf_text = convert_pdf_to_txt(input_path + "/" + filename)
                print("process file from pdf miner: {}".format(filename))
                just_filename = filename.split('.')[0]
                textLines = pdf_text.split('\n')

                if (len(textLines) == 0):
                    print('1' + just_filename)
                    break

                plan_name = textLines[0]
                if (plan_name == ''):
                    print('2' + just_filename)
                    break

                for line in textLines:
                    if 'Plan Type:' in line:
                        plan_type = line.split('Plan Type:')[1]
                        plan_str = plan_name + "_" + plan_type
                        result_dict[just_filename] = [plan_name, plan_type]
                        break

            dfs = []
            for filename in input_files:
                just_filename = filename.split('.')[0]
                if just_filename in result_dict.keys():
                    table = camelot.read_pdf(input_path + "/" + filename, pages='1', line_scale=40, shift_text=[''])
                    print("process file from camelot: {}".format(filename))
                    dfs.append((filename.split('.')[0], table[0].df))
                    
            if not os.path.exists(output_path):
                os.makedirs(output_path)

            for i, item in enumerate(dfs):
                item[1].columns = item[1].iloc[0]
                item[1].drop(item[1].index[0], inplace=True)
                data = item[1].to_json(orient='index')
                item[1].to_json(output_path + "/" + item[0] + ".json", orient='index')

            print("Going into adding plan name and type")
            output_files = os.listdir(output_path)
            for filename in output_files:
                temp = {}
                with open(output_path + "/" + filename, "r") as fr:
                    data = json.load(fr)
                    temp['Plan_Name'] = result_dict[filename.split('.')[0]][0]
                    temp['Plan_Type'] = result_dict[filename.split('.')[0]][1]
                    temp['Page_1'] = data

                with open(output_path + "/" + filename, "w") as fw:
                    json.dump(temp, fw)


        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(push_exp=True)

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = DataPreprocessing()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1],input_path=sys.argv[2],output_path=sys.argv[3])
    else:
        data_prep.start(run_name="")
